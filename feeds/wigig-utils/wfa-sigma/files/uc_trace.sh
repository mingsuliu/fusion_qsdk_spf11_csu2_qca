#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
MYDIR=`dirname $0`
# detect wigig interface
. ./ifc.sh
if [ -z "$WLAN" ]; then
	echo "!!!wigig interface not detected!!!"
	exit
fi
### where is wil6210 debugfs?
D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
RGF_FW_USAGE_2=0x880008
OFF=`$MYDIR/wil_mem -i $WLAN -a $RGF_FW_USAGE_2`
exec $MYDIR/wil_fw_trace --memdump=$D/blob_uc_data --logsize=256 --offset=$(((OFF&0x1FFFFFFF)-0x800000)) --strings=$MYDIR/ucode_strings.bin "$@"
