#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
### where is wil6210 debugfs?
D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
OFF=$(cat $D/RGF_USER_USAGE_1)
exec ./wil_fw_trace --memdump=$D/blob_fw_peri --logsize=1024 --offset=$(((OFF&0x1FFFFFFF)-0x840000)) --strings=fw_strings.bin "$@"
