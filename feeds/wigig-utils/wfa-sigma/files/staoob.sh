#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

BASE_DIR=`dirname $0`
BASE_DIR=`readlink -m $BASE_DIR`
SUPP_CTRL=/var/run/wpa_supplicant
SUPP_CONF=$BASE_DIR/oob_supp.conf
SUPP_LOG=$BASE_DIR/oob_supp.log
WLAN=`$BASE_DIR/ifc.sh`
if [ -z "$WLAN" ]; then
	echo "WIGIG INTERFACE NOT FOUND!!!"
	exit
fi

echo "Killing previous instances of wpa_supplicant/hostapd if any"
killall wpa_supplicant
killall hostapd
sleep 2

echo "Starting wpa_supplicant..."
rm -f $SUPP_CONF
echo "ctrl_interface=$SUPP_CTRL" >> $SUPP_CONF
echo "device_name=Test client" >> $SUPP_CONF
echo "device_type=1-0050F204-1" >> $SUPP_CONF
echo "eapol_version=2" >> $SUPP_CONF
echo "config_methods=display push_button keypad virtual_display physical_display virtual_push_button" >> $SUPP_CONF
echo "freq_list=60480" >> $SUPP_CONF

$BASE_DIR/wpa_supplicant -B -Dnl80211 -i$WLAN -c $SUPP_CONF -f $SUPP_LOG -t -K -ddd
sleep 1
