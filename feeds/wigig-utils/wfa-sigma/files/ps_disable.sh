#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
WMI_PS_DEV_PROFILE_CFG_CMDID=$((16#91C))


function reset_cmd {
	CMD="00: "
}

function append_u8 {
	CMD=`printf "%s%02x" "$CMD" $1`
}

function append_u16 {
	local TMP=`printf "%04x" $1`
	CMD="$CMD ${TMP:2:2}${TMP:0:2}"
}

function append_str {
	local STRHEX=`echo -n $1 | xxd -p`
	CMD=`printf "%s %s" "$CMD $STRHEX"`
}

function append_wmi_header {
	append_u8 0
	append_u8 0
	append_u16 $1
	append_u8 0
	append_u8 0
	append_u8 0
	append_u8 0
}

function send_wmi {
	### where is wil6210 debugfs?
	D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
	echo "$CMD" | xxd -r -c 256 > $D/wmi_send
	#DEBUGGING
	#echo "$CMD" >> wmi.txt
	#echo "$CMD" | xxd -r -c 256 >> wmi.bin
	#TODO we should implement wmi_call in debugfs so we don't need to
	#do arbitrary sleep
	sleep 0.5
}

function wmi_ps_disable {
	reset_cmd
	append_wmi_header $WMI_PS_DEV_PROFILE_CFG_CMDID
	#ps_profile
	append_u8 1
	#reserved[0,1,2]
	append_u8 0
	append_u8 0
	append_u8 0
	send_wmi
}


wmi_ps_disable
