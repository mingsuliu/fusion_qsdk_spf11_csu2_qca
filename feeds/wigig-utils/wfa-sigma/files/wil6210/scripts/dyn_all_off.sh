#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

# turns off all dynamic logging
# require root permissions

echo "module wil6210 -p" >  /sys/kernel/debug/dynamic_debug/control
