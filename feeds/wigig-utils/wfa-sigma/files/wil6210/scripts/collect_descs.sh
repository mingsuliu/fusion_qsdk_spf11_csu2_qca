#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

# collect_descs.sh [vring]
# collect all descriptors for the given vring
# vring should be in range [0..24], where 24 means Rx
# default is 24 (Rx)
# script 'txdesc.sh' should be in the same directory
# requires root permissions

RING=${1-24}

D=$(dirname $0)

for i in {0..127}; do $D/txdesc.sh $RING $i; done
