#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

# turns on all dynamic logging in the "WMI" category
# require root permissions

echo "module wil6210 format 'DBG[ WMI]'" +p >  /sys/kernel/debug/dynamic_debug/control
