#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
### parameter - file with WMI command to send
### file should contain WMI command starting from
### WMI header wil6210_mbox_hdr_wmi
F=$1
### where is wil6210 debugfs?
D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
cat $F > $D/wmi_send
