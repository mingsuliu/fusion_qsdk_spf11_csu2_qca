#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
### set new interrupt threshold
### parameter - new threshold
X=$1
### where is wil6210 debugfs?
D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
echo 0 > $D/ITR_CNT/CTL
echo $X > $D/ITR_CNT/TRSH
echo 1 > $D/ITR_CNT/CTL
