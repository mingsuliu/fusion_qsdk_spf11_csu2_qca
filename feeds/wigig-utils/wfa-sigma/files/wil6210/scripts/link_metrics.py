#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
#This script requires the following to be installed:
#sudo apt-get install python-matplotlib
#sudo apt-get install python-pip
#sudo  apt-get install libfreetype6-dev
#sudo apt-get install python-dev
#pip install drawnow

import warnings
import numpy
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from drawnow import *
import subprocess
import re
from time import sleep
import thread

script_ver="1.0"

wil6210_debugfs_path = subprocess.check_output('find /sys/kernel/debug/ieee80211 -name wil6210',shell=True)

g_shared_mem_ipc=0x91f078

tx_ageing_threshold_usec_addr = g_shared_mem_ipc+0x8
bad_beacons_num_threshold_addr = g_shared_mem_ipc + 0x1c
tx_ageing_usec_0_addr = g_shared_mem_ipc + 0x164
bad_consecutive_bcons_cnt_0_addr = g_shared_mem_ipc + 0x1e4
consecutive_bcons_atim_fail_0_addr = 0x9405ac
brp_ina_snr_addr = 0x940ac8

val = subprocess.check_output('echo ' + hex(tx_ageing_threshold_usec_addr) + ' > ' + wil6210_debugfs_path.rstrip() + '/mem_addr', shell=True)
val = subprocess.check_output('cat ' + wil6210_debugfs_path.rstrip() + '/mem_val | cut -c 16-', shell=True)

aging = []
snr = []
plt.ion() #Tell matplotlib you want interactive mode to plot live data
cnt=0
status=""
loop_cnt=0

def makeFigAge(): #Create a function that makes our desired plot
	global status
	fig1 = plt.figure(1)
	ax1 = fig1.add_subplot(2, 1, 2)
	ax2 = fig1.add_subplot(2, 1, 1)

	ax1.plot(snr, '', label=status, linestyle='-')
	ax2.plot(aging, '', label=status, linestyle='-')

	ax1.set_title('\n SNR') #Plot the title
	ax1.grid(True)
	ax2.set_title('TX aging') #Plot the title
	ax2.grid(True)

	try:
		tmp_st = subprocess.check_output('ifconfig wlan0|grep RUNNING', shell=True)
	except subprocess.CalledProcessError as e:
		tmp_st = ""
	if tmp_st.rstrip() == "":
		status="11AD Disconnected"
	else:
		status="11AD Connected"
	plt.legend(loc='upper left') #Plot the status

	plt.ylim(-10000, 201000)

	l = ax2.axhline(y=200000 , color='#d62728', alpha=0.5, linewidth=25)

def collectData():
	global aging
	global snr
	global cnt
	while True: #While loop that loops forever
		new_val = "0x0"

		val = subprocess.check_output('echo ' + hex(tx_ageing_usec_0_addr) + ' > ' + wil6210_debugfs_path.rstrip() + '/mem_addr', shell=True)
		val = subprocess.check_output('cat ' + wil6210_debugfs_path.rstrip() + '/mem_val | cut -c 16-', shell=True)
		age_val = int(val.rstrip(),16)
		aging.append(age_val) #Build our temp array by appending aging readings

		val = subprocess.check_output('echo ' + hex(brp_ina_snr_addr) + ' > ' + wil6210_debugfs_path.rstrip() + '/mem_addr', shell=True)
		val = subprocess.check_output('cat ' + wil6210_debugfs_path.rstrip() + '/mem_val | cut -c 16-', shell=True)
		match = re.search("0x[\w]{2}(\w\w)[\w]{4}",val.rstrip(),re.MULTILINE)
		if (match is not None):
			new_val = "0x" + match.group(1)
			snr_val = int(new_val.rstrip(),16) + 5
		snr.append(snr_val)

		sleep(0.01)
		cnt=cnt+1
		if(cnt>400): #If you have 400 or more points, delete the first one from the array
			aging.pop(0) #This allows us to just see the last 400 data
			snr.pop(0)

thread.start_new_thread(collectData, ())

while True: #While loop that loops forever
	plt.pause(.5)
	drawnow(makeFigAge)
