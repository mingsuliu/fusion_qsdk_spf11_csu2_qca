#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
# Display Tx/Rx descriptor content
# Vring index, 0..24 for Tx, 25 for Rx
V=$1
# Descriptor index
I=$2
### where is wil6210 debugfs?
D=$(find /sys/kernel/debug/ieee80211/ -name wil6210)
echo $V > $D/vring_index
echo $I > $D/desc_index
cat $D/desc
