#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

# OOB configuration
SSID=ap_oob
SECURE=1
PSK=12345678

###################

BASE_DIR=`dirname $0`
BASE_DIR=`readlink -m $BASE_DIR`
HOSTAPD_CTRL=/var/run/hostapd
HOSTAPD_CONF=$BASE_DIR/oob_hostapd.conf
HOSTAPD_LOG=$BASE_DIR/oob_hostapd.log
WLAN=`$BASE_DIR/ifc.sh`
if [ -z "$WLAN" ]; then
	echo "WIGIG INTERFACE NOT FOUND!!!"
	exit
fi

echo "Killing previous instances of wpa_supplicant/hostapd if any"
killall wpa_supplicant
killall hostapd
sleep 2

echo "Starting hostapd..."
rm -f $HOSTAPD_CONF
echo "hw_mode=ad" >> $HOSTAPD_CONF
echo "interface=$WLAN" >> $HOSTAPD_CONF
echo "channel=2" >> $HOSTAPD_CONF
echo "ctrl_interface=$HOSTAPD_CTRL" >> $HOSTAPD_CONF
echo "ssid=$SSID" >> $HOSTAPD_CONF
echo "beacon_int=100" >> $HOSTAPD_CONF
if [ "$SECURE" = "1" ];
then
	echo "wpa=2" >> $HOSTAPD_CONF
	echo "wpa_key_mgmt=WPA-PSK" >> $HOSTAPD_CONF
	echo "wpa_pairwise=GCMP" >> $HOSTAPD_CONF
	echo "wpa_passphrase=$PSK" >> $HOSTAPD_CONF
fi

# WPS parameters
echo "wps_state=2" >> $HOSTAPD_CONF
echo "device_name=QCA AP" >> $HOSTAPD_CONF
echo "manufacturer=QCA" >> $HOSTAPD_CONF
echo "device_type=6-0050F204-1" >> $HOSTAPD_CONF
echo "config_methods=label virtual_display physical_display virtual_push_button keypad" >> $HOSTAPD_CONF
echo "ap_pin=12345670" >> $HOSTAPD_CONF
echo "friendly_name=QCA Access Point" >> $HOSTAPD_CONF

$BASE_DIR/hostapd -dd -f $HOSTAPD_LOG $HOSTAPD_CONF &
sleep 1
