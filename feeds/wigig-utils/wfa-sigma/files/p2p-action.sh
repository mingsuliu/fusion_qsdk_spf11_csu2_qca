#!/bin/sh
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

IFNAME=$1
CMD=$2


echo " "
echo " ---------------------------------------------------------------------------"
echo "  P2P-action.sh $1 $2 $3 $4"

kill_daemon() {
    NAME=$1
    PF=$2

    if [ ! -r $PF ]; then
	return
    fi

    PID=`cat $PF`
    if [ $PID -gt 0 ]; then
	if ps $PID | grep -q $NAME; then
	    kill $PID
	fi
    fi
    rm $PF
}

if [ "$CMD" = "P2P-GROUP-STARTED" ]; then
    GIFNAME=$3
    if [ "$4" = "GO" ]; then
	kill_daemon dhclient /var/run/dhclient-$GIFNAME.pid
	rm /var/run/dhclient.leases-$GIFNAME
	kill_daemon dnsmasq /var/run/dnsmasq.pid-$GIFNAME
	ifconfig $GIFNAME 192.165.100.1 up
	echo "GO start dnsmasq"
	dnsmasq -x /var/run/dnsmasq.pid-$GIFNAME \
	    -i $GIFNAME \
	    -F192.165.100.11,192.165.100.99
    fi
    if [ "$4" = "client" ]; then
	kill_daemon dhclient /var/run/dhclient-$GIFNAME.pid
	rm /var/run/dhclient.leases-$GIFNAME
	kill_daemon dnsmasq /var/run/dnsmasq.pid-$GIFNAME
	echo "GC start dhclient"
	dhclient -pf /var/run/dhclient-$GIFNAME.pid \
	    -cf /etc/dhcp/dhclient.conf \
	    -lf /var/run/dhclient.leases-$GIFNAME \
	    -sf /usr/sbin/dhclient-script \
	    -nw \
	    $GIFNAME

#	    -sf /etc/dhcp3/dhclient-script \
    fi
fi

if [ "$CMD" = "P2P-GROUP-REMOVED" ]; then
    GIFNAME=$3
    if [ "$4" = "GO" ]; then
	echo "GO stop dnsmasq "
	kill_daemon dnsmasq /var/run/dnsmasq.pid-$GIFNAME
	ifconfig $GIFNAME 0.0.0.0
    fi
    if [ "$4" = "client" ]; then
	echo "GC stop dhclient "
	kill_daemon dhclient /var/run/dhclient-$GIFNAME.pid
	rm /var/run/dhclient.leases-$GIFNAME
	ifconfig $GIFNAME 0.0.0.0
    fi
fi

if [ "$CMD" = "P2P-CROSS-CONNECT-ENABLE" ]; then
    GIFNAME=$3
    UPLINK=$4
    # enable NAT/masquarade $GIFNAME -> $UPLINK
    iptables -P FORWARD DROP
    iptables -t nat -A POSTROUTING -o $UPLINK -j MASQUERADE
    iptables -A FORWARD -i $UPLINK -o $GIFNAME -m state --state RELATED,ESTABLISHED -j ACCEPT
    iptables -A FORWARD -i $GIFNAME -o $UPLINK -j ACCEPT
    sysctl net.ipv4.ip_forward=1
fi

if [ "$CMD" = "P2P-CROSS-CONNECT-DISABLE" ]; then
    GIFNAME=$3
    UPLINK=$4
    # disable NAT/masquarade $GIFNAME -> $UPLINK
    sysctl net.ipv4.ip_forward=0
    iptables -t nat -D POSTROUTING -o $UPLINK -j MASQUERADE
    iptables -D FORWARD -i $UPLINK -o $GIFNAME -m state --state RELATED,ESTABLISHED -j ACCEPT
    iptables -D FORWARD -i $GIFNAME -o $UPLINK -j ACCEPT
fi

echo " ---------------------------------------------------------------------------"
