#!/bin/bash
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#
BASE_DIR=`dirname $0`
LOG_DIR="/tmp/sigma_test/wfa"
WITH_UCODE="0"

for var in "$@"
do
	if [ $var = "ucode" ];
	then
		echo "** With UCODE logs"
		WITH_UCODE="1"
	fi
done

function start_logging {
	mkdir -p $LOG_DIR
	#tail -f /var/log/kern.log > $BASE_DIR/kmsg.txt &
	dmesg -w --color=never > $LOG_DIR/kmsg.txt &
	./fw_trace.sh 2>&1 > $LOG_DIR/fw.log &
	if [ $WITH_UCODE = "1" ];
	then
		./uc_trace.sh 2>&1 > $LOG_DIR/uc.log &
	fi
}

function stop_logging {
	# kill the kernel logging (kmsg.txt)
	#TMPPID=`ps ax | grep kern.log | grep -v grep`
	TMPPID=`ps ax | grep dmesg | grep -v grep`
	TMPPID=($TMPPID)
	TMPPID=${TMPPID[0]}
	if [ -n "$TMPPID" ]; then
		echo "killing kmsg.txt logger at $TMPPID"
		kill $TMPPID
	else
		echo "kmsg.txt logger not running"
	fi
	# kill the fw logger (fw.log)
	TMPPID=`ps ax | grep blob_fw_peri | grep -v grep`
	TMPPID=($TMPPID)
	TMPPID=${TMPPID[0]}
	if [ -n "$TMPPID" ]; then
		echo "killing fw.log logger at $TMPPID"
		kill $TMPPID
	else
		echo "fw.log logger not running"
	fi
	# kill the ucode logger (uc.log)
	TMPPID=`ps ax | grep blob_uc_data | grep -v grep`
	TMPPID=($TMPPID)
	TMPPID=${TMPPID[0]}
	if [ -n "$TMPPID" ]; then
		echo "killing uc.log logger at $TMPPID"
		kill $TMPPID
	else
		echo "uc.log logger not running"
	fi
}

COMMAND=$1
case $COMMAND in
start)
	stop_logging
	start_logging
;;
stop)
	stop_logging
;;
restart)
;;
*)
	echo Usage: $0 [start|stop]
;;
esac
