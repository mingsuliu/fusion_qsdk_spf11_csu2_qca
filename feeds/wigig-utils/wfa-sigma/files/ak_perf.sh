#!/bin/sh
#
# Copyright (c) 2018 Qualcomm Technologies, Inc.
# All Rights Reserved.
# Confidential and Proprietary - Qualcomm Technologies, Inc.
#

echo 2 > /sys/class/net/wlan0/queues/rx-0/rps_cpus
echo 2 > /sys/class/net/wlan1/queues/rx-0/rps_cpus
echo ef > /sys/class/net/bond0/queues/rx-0/rps_cpus
echo 8388608 > /proc/sys/net/core/rmem_max
echo 1048576 > /proc/sys/net/core/rmem_default
echo 8388608 > /proc/sys/net/core/wmem_max
echo 1048576 > /proc/sys/net/core/wmem_default
echo 8388608 8388608 8388608 > /proc/sys/net/ipv4/tcp_mem
echo 1048576 6291456 8388608 > /proc/sys/net/ipv4/tcp_rmem
echo 1048576 6291456 8388608 > /proc/sys/net/ipv4/tcp_wmem
echo 8800000 > /proc/sys/net/ipv4/tcp_limit_output_bytes
echo 8388608 8388608 8388608 > /proc/sys/net/ipv4/udp_mem
echo 1048576 > /proc/sys/net/ipv4/udp_rmem_min
echo 1048576 > /proc/sys/net/ipv4/udp_wmem_min
echo 1 > /proc/sys/net/ipv4/tcp_window_scaling
echo 1 > /proc/sys/net/ipv4/tcp_timestamps
echo 1 > /proc/sys/net/ipv4/tcp_sack
echo 1 > /proc/sys/net/ipv4/tcp_no_metrics_save
echo 5000 > /proc/sys/net/core/netdev_max_backlog

echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo performance  > /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor

