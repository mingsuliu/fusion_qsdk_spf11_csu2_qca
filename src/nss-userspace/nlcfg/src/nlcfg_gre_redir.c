/*
 * Copyright (c) 2019 Qualcomm Technologies, Inc.
 *
 * All Rights Reserved.
 * Confidential and Proprietary - Qualcomm Technologies, Inc.
 */

/*
 * @file NLCFG gre_redir handler
 */
#include <string.h>
#include <arpa/inet.h>
#include <ctype.h>
#include <nss_def.h>
#include <nss_nlbase.h>
#include <nss_nl_if.h>
#include "nlcfg_hlos.h"
#include "nlcfg_param.h"
#include "nss_nlcmn_if.h"
#include "nlcfg_gre_redir.h"
#include "nss_nlgre_redir_if.h"

/*
 * Function prototypes
 */
static int nlcfg_gre_redir_create_tun(struct nlcfg_param *, struct nlcfg_param_in *);
static int nlcfg_gre_redir_destroy_tun(struct nlcfg_param *, struct nlcfg_param_in *);
static int nlcfg_gre_redir_map_vap(struct nlcfg_param *, struct nlcfg_param_in *);
static int nlcfg_gre_redir_unmap_vap(struct nlcfg_param *, struct nlcfg_param_in *);
static int nlcfg_gre_redir_set_next_hop(struct nlcfg_param *, struct nlcfg_param_in *);

/*
 * Create tunnel parameters
 */
struct nlcfg_param create_tun_params[NLCFG_GRE_REDIR_CREATE_TUN_PARAM_MAX] = {
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_CREATE_TUN_PARAM_IPTYPE, "iptype="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_CREATE_TUN_PARAM_SIP, "sip="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_CREATE_TUN_PARAM_DIP, "dip="),
};

/*
 * Delete tunnel parameters
 */
struct nlcfg_param destroy_tun_params[NLCFG_GRE_REDIR_DESTROY_TUN_PARAM_MAX] = {
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_DESTROY_TUN_PARAM_NETDEV, "netdev="),
};

/*
 * Map parameters
 */
struct nlcfg_param map_params[NLCFG_GRE_REDIR_MAP_PARAM_MAX] = {
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_MAP_PARAM_VAP_NSS_IF, "vap_nss_if="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_MAP_PARAM_RID, "rid="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_MAP_PARAM_VID, "vid="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_MAP_PARAM_TUN_TYPE, "tun_type="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_MAP_PARAM_SA_PAT, "sa_pat="),
};

/*
 * Unmap parameters
 */
struct nlcfg_param unmap_params[NLCFG_GRE_REDIR_UNMAP_PARAM_MAX] = {
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_UNMAP_PARAM_VAP_NSS_IF, "vap_nss_if="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_UNMAP_PARAM_RID, "rid="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_UNMAP_PARAM_VID, "vid="),
};

/*
 * Set_next hop parameters
 */
struct nlcfg_param set_next_params[NLCFG_GRE_REDIR_SET_NEXT_PARAM_MAX] = {
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_SET_NEXT_PARAM_DEV_NAME, "dev_name="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_SET_NEXT_PARAM_NEXT_DEV_NAME, "next_dev_name="),
	NLCFG_PARAM_INIT(NLCFG_GRE_REDIR_SET_NEXT_PARAM_MODE, "mode="),
};

/*
 * Gre-redir parameters
 */
struct nlcfg_param nlcfg_gre_redir_params[NLCFG_GRE_REDIR_CMD_TYPE_MAX] = {
	NLCFG_PARAMLIST_INIT("cmd=create", create_tun_params, nlcfg_gre_redir_create_tun),
	NLCFG_PARAMLIST_INIT("cmd=destroy", destroy_tun_params, nlcfg_gre_redir_destroy_tun),
	NLCFG_PARAMLIST_INIT("cmd=map", map_params, nlcfg_gre_redir_map_vap),
	NLCFG_PARAMLIST_INIT("cmd=unmap", unmap_params, nlcfg_gre_redir_unmap_vap),
	NLCFG_PARAMLIST_INIT("cmd=set_next", set_next_params, nlcfg_gre_redir_set_next_hop),
};

static struct nss_nlgre_redir_ctx nss_ctx;

/*
 * nlcfg_gre_redir_create_tunnel()
 * 	Handles GRE_REDIR tunnel creation
 */
static int nlcfg_gre_redir_create_tun(struct nlcfg_param *param, struct nlcfg_param_in *match)
{
	struct nss_nlgre_redir_rule nl_msg = {{0}};
	uint32_t size;
	int error;

	if (!match || !param) {
		return -EINVAL;
	}

	/*
	 *  open the NSS GRE_REDIR NL socket
	 */
	error = nss_nlgre_redir_sock_open(&nss_ctx, NULL, NULL);
	if (error < 0) {
		nlcfg_log_error("Unable to open the socket\n");
		return -ENOMEM;
	}

	/*
	 * Initialize the rule
	 */
	nss_nlgre_redir_init_rule(&nss_ctx, &nl_msg, NULL, NULL, NLCFG_GRE_REDIR_CMD_TYPE_CREATE_TUN);

	/*
	 *  Iterate through the args to extract the parameters
	 */
        error = nlcfg_param_iter_tbl(param, match);
	if (error) {
		nlcfg_log_arg_error(param);
		goto done;
	}

	/*
	 * Extract the type of IP address used
	 */
	struct nlcfg_param *sub_params = &param->sub_params[NLCFG_GRE_REDIR_CREATE_TUN_PARAM_IPTYPE];
	error = nlcfg_param_get_int(sub_params->data, sizeof(uint32_t), &nl_msg.msg.create.iptype);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	size = (nl_msg.msg.create.iptype == 1 ? sizeof(struct in_addr) : sizeof(struct in6_addr));

	/*
	 * Extract the source IP address
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_CREATE_TUN_PARAM_SIP];
	error = nlcfg_param_get_ipaddr_ntoh(sub_params->data, size, nl_msg.msg.create.sip);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the destination IP address
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_CREATE_TUN_PARAM_DIP];
	error = nlcfg_param_get_ipaddr_ntoh(sub_params->data, size, nl_msg.msg.create.dip);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Send the gre_redir msg to kernel using netlink socket
	 */
	error = nss_nlgre_redir_sock_send(&nss_ctx, &nl_msg);
	if (error < 0) {
		nlcfg_log_error("Failed to create tunnel error:%d\n", error);
		goto done;
	}

	nlcfg_log_info("Tunnel create message sent successfully\n");
done:
	/*
	 * close the socket
	 */
	nss_nlgre_redir_sock_close(&nss_ctx);
	return error;
}

/*
 * nlcfg_gre_redir_destroy_tun()
 *	Handle GRE_REDIR tunnel delete
 */
static int nlcfg_gre_redir_destroy_tun(struct nlcfg_param *param, struct nlcfg_param_in *match)
{
	struct nss_nlgre_redir_rule nl_msg = {{0}};
	int error;

	if (!match || !param) {
		return -EINVAL;
	}

        /*
	 * open the NSS GRE_REDIR NL socket
	 */
	error = nss_nlgre_redir_sock_open(&nss_ctx, NULL, NULL);
	if (error < 0) {
		nlcfg_log_error("Unable to open the socket\n");
		return -ENOMEM;
	}

	nss_nlgre_redir_init_rule(&nss_ctx, &nl_msg, NULL, NULL, NLCFG_GRE_REDIR_CMD_TYPE_DESTROY_TUN);

	/*
	 * Iterate through the parameters table to identify the
	 * matched arguments and populate the argument list.
	 */
	error = nlcfg_param_iter_tbl(param, match);
	if (error) {
		nlcfg_log_arg_error(param);
		goto done;
	}

	/*
	 *  Extract the netdev parameter
	 */
	struct nlcfg_param *sub_params = &param->sub_params[NLCFG_GRE_REDIR_DESTROY_TUN_PARAM_NETDEV];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.destroy.netdev), nl_msg.msg.destroy.netdev);
	if (error < 0) {
		nlcfg_log_error("Not a valid device type.");
		goto done;
	}

	/*
	 *  Send tunnel destroy message
	 */
	error = nss_nlgre_redir_sock_send(&nss_ctx, &nl_msg);
	if (error < 0) {
		nlcfg_log_error("%s: Failed to destroy tunnel error:%d\n", nl_msg.msg.destroy.netdev, error);
		goto done;
	}

	nlcfg_log_info("%s: Destroy tunnel message sent successfully\n", nl_msg.msg.destroy.netdev);
done:
	/*
	 * close the socket
	 */
	nss_nlgre_redir_sock_close(&nss_ctx);
	return error;
}

/*
 * nlcfg_gre_redir_map_vap
 * 	HANDLES gre_redir map
 */
static int nlcfg_gre_redir_map_vap(struct nlcfg_param *param, struct nlcfg_param_in *match)
{
	struct nss_nlgre_redir_rule nl_msg = {{0}};
	int error;

	if (!match || !param) {
		return -EINVAL;
	}

        /*
	 * open the NSS GRE_REDIR NL socket
	 */
	error = nss_nlgre_redir_sock_open(&nss_ctx, NULL, NULL);
	if (error < 0) {
		nlcfg_log_error("Unable to open the socket\n");
		return -ENOMEM;
	}

	/*
	 *  Initialize the rule
	 */
	nss_nlgre_redir_init_rule(&nss_ctx, &nl_msg, NULL, NULL, NLCFG_GRE_REDIR_CMD_TYPE_MAP);

	/*
	 *  Iterate through the args to extract the parameters
	 */
        error = nlcfg_param_iter_tbl(param, match);
	if (error) {
		nlcfg_log_arg_error(param);
		goto done;
	}

	/*
	 * Extract the NSS interface
	 */
	struct nlcfg_param *sub_params = &param->sub_params[NLCFG_GRE_REDIR_MAP_PARAM_VAP_NSS_IF];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.map.vap_nss_if), nl_msg.msg.map.vap_nss_if);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the radio ID
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_MAP_PARAM_RID];
	error = nlcfg_param_get_int(sub_params->data, sizeof(nl_msg.msg.map.rid), &nl_msg.msg.map.rid);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 * Extract the VAP ID
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_MAP_PARAM_VID];
	error = nlcfg_param_get_int(sub_params->data, sizeof(nl_msg.msg.map.vid), &nl_msg.msg.map.vid);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the tunnel type
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_MAP_PARAM_TUN_TYPE];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.map.tun_type), nl_msg.msg.map.tun_type);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the Security association parameter
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_MAP_PARAM_SA_PAT];
	error = nlcfg_param_get_int(sub_params->data, sizeof(nl_msg.msg.map.ipsec_sa_pattern), &nl_msg.msg.map.ipsec_sa_pattern);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Send the message through netlink socket
	 */
	error = nss_nlgre_redir_sock_send(&nss_ctx, &nl_msg);
	if (error < 0) {
		nlcfg_log_error("%s: Failed to map:%d\n", nl_msg.msg.map.vap_nss_if, error);
		goto done;
	}

	nlcfg_log_info("%s: map message sent successfully\n", nl_msg.msg.map.vap_nss_if);
done:
	/*
	 * Close the socket
	 */
	nss_nlgre_redir_sock_close(&nss_ctx);
	return error;
}

/*
 * nlcfg_gre_redir_unmap
 * 	HANDLES gre_redir map
 */
static int nlcfg_gre_redir_unmap_vap(struct nlcfg_param *param, struct nlcfg_param_in *match)
{
	struct nss_nlgre_redir_rule nl_msg = {{0}};
	int error;

	if (!match || !param) {
		return -EINVAL;
	}

        /*
	 * open the NSS GRE_REDIR NL socket
	 */
	error = nss_nlgre_redir_sock_open(&nss_ctx, NULL, NULL);
	if (error < 0) {
		nlcfg_log_error("Unable to open the socket\n");
		return -ENOMEM;
	}

	/*
	 *  Initialize the rule
	 */
	nss_nlgre_redir_init_rule(&nss_ctx, &nl_msg, NULL, NULL, NLCFG_GRE_REDIR_CMD_TYPE_UNMAP);

	/*
	 *  Iterate through the args to extract the parameters
	 */
        error = nlcfg_param_iter_tbl(param, match);
	if (error) {
		nlcfg_log_arg_error(param);
		goto done;
	}

	/*
	 *  Extract the NSS interface
	 */
	struct nlcfg_param *sub_params = &param->sub_params[NLCFG_GRE_REDIR_UNMAP_PARAM_VAP_NSS_IF];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.unmap.vap_nss_if), nl_msg.msg.unmap.vap_nss_if);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the radio ID
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_UNMAP_PARAM_RID];
	error = nlcfg_param_get_int(sub_params->data, sizeof(nl_msg.msg.unmap.rid), &nl_msg.msg.unmap.rid);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the VID
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_UNMAP_PARAM_VID];
	error = nlcfg_param_get_int(sub_params->data, sizeof(nl_msg.msg.unmap.vid), &nl_msg.msg.unmap.vid);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 * Send the message through netlink socket
	 */
	error = nss_nlgre_redir_sock_send(&nss_ctx, &nl_msg);
	if (error < 0) {
		nlcfg_log_error("%s: Failed to unmap:%d\n", nl_msg.msg.unmap.vap_nss_if, error);
		goto done;
	}

	nlcfg_log_info("%s: Unmap message sent successfully\n", nl_msg.msg.unmap.vap_nss_if);
done:
	/*
	 * Close the socket
	 */
	nss_nlgre_redir_sock_close(&nss_ctx);
	return error;
}

/*
 * nlcfg_gre_redir_set_next_hop
 * 	Sets the next hop for vap_nss_if
 */
static int nlcfg_gre_redir_set_next_hop(struct nlcfg_param *param, struct nlcfg_param_in *match)
{
	struct nss_nlgre_redir_rule nl_msg = {{0}};
	int error;

	if (!match || !param) {
		return -EINVAL;
	}

        /*
	 * open the NSS GRE_REDIR NL socket
	 */
	error = nss_nlgre_redir_sock_open(&nss_ctx, NULL, NULL);
	if (error < 0) {
		nlcfg_log_error("Unable to open the socket\n");
		return -ENOMEM;
	}

	/*
	 *  Initialize the rule
	 */
	nss_nlgre_redir_init_rule(&nss_ctx, &nl_msg, NULL, NULL, NLCFG_GRE_REDIR_CMD_TYPE_SET_NEXT_HOP);

	/*
	 * Iterate through the args to extract the parameters
	 */
        error = nlcfg_param_iter_tbl(param, match);
	if (error) {
		nlcfg_log_arg_error(param);
		goto done;
	}

	/*
	 *  Extract the device name
	 */
	struct nlcfg_param *sub_params = &param->sub_params[NLCFG_GRE_REDIR_SET_NEXT_PARAM_DEV_NAME];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.snext.dev_name), nl_msg.msg.snext.dev_name);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the tunnel device name
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_SET_NEXT_PARAM_NEXT_DEV_NAME];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.snext.next_dev_name), nl_msg.msg.snext.next_dev_name);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Extract the mode
	 */
	sub_params = &param->sub_params[NLCFG_GRE_REDIR_SET_NEXT_PARAM_MODE];
	error = nlcfg_param_get_str(sub_params->data, sizeof(nl_msg.msg.snext.mode), nl_msg.msg.snext.mode);
	if (error < 0) {
		nlcfg_log_data_error(sub_params);
		goto done;
	}

	/*
	 *  Send the message through netlink socket
	 */
	error = nss_nlgre_redir_sock_send(&nss_ctx, &nl_msg);
	if (error < 0) {
		nlcfg_log_error("Failed to set next hop:%d\n", error);
		goto done;
	}

	nlcfg_log_info("Set_next message sent successfully\n");
done:
	/*
	 * Close the socket
	 */
	nss_nlgre_redir_sock_close(&nss_ctx);
	return error;
}

