/*
* Copyright (c) 2019 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/



#include "DeviceLoggingContext.h"
#include "ChunkConsumer.h"
#include "Host.h"
#include "DeviceManager.h"
#include "Device.h"
using namespace std;

namespace log_collector
{
    DeviceLoggingContext::DeviceLoggingContext(const std::string& deviceName)
        : m_deviceName(deviceName)
    {
        shared_ptr<Device> device = Host::GetHost().GetDeviceManager().GetDeviceByName(m_deviceName);
        if (!device)
        {
            LOG_ERROR << "Invalid device name provided " << m_deviceName << endl;
            return;
        }
        unique_lock<mutex> lock(device->m_mutex);
        const auto fwStateProvider = device->GetFwStateProvider();
        lock.unlock();
        m_lastFwVersion = fwStateProvider->GetFwVersion();
        m_lastFwTimestamp = fwStateProvider->GetFwTimestamp();
        m_lastuCodeTimestamp = fwStateProvider->GetUcodeTimestamp();
        LOG_DEBUG << "DeviceLoggingContext ctor: m_lastFwVersion: " << m_lastFwVersion
            << " m_lastFwTimestamp: " << m_lastFwTimestamp
            << " m_lastuCodeTimestamp: " << m_lastuCodeTimestamp << endl;
        m_fwLogCollector = make_shared<log_collector::NewLogCollector>(m_deviceName, log_collector::CPU_TYPE_FW);
        m_uCodeLogCollector = make_shared<log_collector::NewLogCollector>(m_deviceName, log_collector::CPU_TYPE_UCODE);
    }


    DeviceLoggingContext::~DeviceLoggingContext() = default;

    void DeviceLoggingContext::Resume()
    {
        m_fwLogCollector->RegisterPoller();
        m_uCodeLogCollector->RegisterPoller();

        LOG_DEBUG << "Resuming recording for: " << m_deviceName << endl;
        if (!PrepareForLogging())
        {
            LOG_ERROR << "Failed to Prepare recording. Configuration might be invalid. Fix configuration and start recording again." << endl;
        }

        if (!VerifyFwVersion())
        {
            // PrepareForLogging return false when there is an error we can't recover from.
            // PrepareForLogging return true even if FW is not ready so we can try and prepare again when ready.
            LOG_WARNING << "Fw is not ready yet" << endl;
            return;
        }

        m_fwLogCollector->Resume();
        m_uCodeLogCollector->Resume();
    }

    bool DeviceLoggingContext::PrepareForLogging()
    {
        bool resFw = m_fwLogCollector->PrepareForLogging();
        if (!resFw)
        {
            /*m_fwLogCollector->DeactivateLogging(RawPublisher);
            m_fwLogCollector->DeactivateLogging(XmlRecorder);
            m_fwLogCollector->DeactivateLogging(TxtRecorder);*/
        }
        bool resuCode = m_uCodeLogCollector->PrepareForLogging();
        if (!resuCode)
        {
            /*m_uCodeLogCollector->DeactivateLogging(RawPublisher);
            m_uCodeLogCollector->DeactivateLogging(XmlRecorder);
            m_uCodeLogCollector->DeactivateLogging(TxtRecorder);*/
        }
        return resFw && resuCode;
    }

    void DeviceLoggingContext::Pause()
    {
        m_fwLogCollector->UnRegisterPoller();
        m_uCodeLogCollector->UnRegisterPoller();
        m_fwLogCollector->Pause();
        m_uCodeLogCollector->Pause();
    }


    bool DeviceLoggingContext::VerifyFwVersion()
    {
        shared_ptr<Device> device = Host::GetHost().GetDeviceManager().GetDeviceByName(m_deviceName);
        if (!device)
        {

            LOG_ERROR << "Invalid device name provided " << m_deviceName << endl;
            return false; // we want to prepare recording so the validity wil be updated.
        }
        unique_lock<mutex> lock(device->m_mutex);
        const FwStateProvider* fwStateProvider = device->GetFwStateProvider();
        lock.unlock();

        wil_fw_state currState = fwStateProvider->GetFwHealthState();

        if (currState != wil_fw_state::WIL_FW_STATE_ERROR_BEFORE_READY && currState != wil_fw_state::WIL_FW_STATE_READY)
        {
            LOG_DEBUG << " Fw not ready FW state is: " << static_cast<int>(currState) << endl;
            return false;
        }

        FwVersion tmpFwVersion = fwStateProvider->GetFwVersion();
        FwTimestamp tmpFwTimestamp = fwStateProvider->GetFwTimestamp();
        FwTimestamp tmuCodeTimestamp = fwStateProvider->GetUcodeTimestamp();
        if (!(m_lastFwVersion == tmpFwVersion && m_lastFwTimestamp == tmpFwTimestamp && m_lastuCodeTimestamp == tmuCodeTimestamp))
        {
            LOG_INFO << "FW changed " << m_deviceName << "from: " << m_lastFwVersion << "to: " << tmpFwVersion << endl;

            m_lastFwVersion = tmpFwVersion;
            m_lastFwTimestamp = tmpFwTimestamp;
            m_lastuCodeTimestamp = tmuCodeTimestamp;
        }

        return true;
    }
}
