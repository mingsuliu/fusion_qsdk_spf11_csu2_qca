/*
* Copyright (c) 2019 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/


#pragma once

#include <memory>
#include <string>
#include "NewLogCollector.h"

class Device;


namespace log_collector
{

    class DeviceLoggingContext
    {
    public:
        explicit DeviceLoggingContext(const std::string &deviceName);
        ~DeviceLoggingContext();
        //TODOR: make member.
        std::shared_ptr<NewLogCollector> GetFwLogCollector() { return m_fwLogCollector; }
        std::shared_ptr<NewLogCollector> GetuCodeLogCollector() { return m_uCodeLogCollector; }
        bool VerifyFwVersion();
        void Resume();
        bool PrepareForLogging();
        void Pause();
    private:
        const std::string m_deviceName;
        std::shared_ptr<NewLogCollector> m_fwLogCollector;
        std::shared_ptr<NewLogCollector> m_uCodeLogCollector;
        FwVersion m_lastFwVersion;
        FwTimestamp m_lastFwTimestamp;
        FwTimestamp m_lastuCodeTimestamp;

    };

}


