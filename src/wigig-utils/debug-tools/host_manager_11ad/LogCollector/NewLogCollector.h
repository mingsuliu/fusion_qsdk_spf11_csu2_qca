/*
* Copyright (c) 2019 Qualcomm Technologies, Inc.
* All Rights Reserved.
* Confidential and Proprietary - Qualcomm Technologies, Inc.
*/

#pragma once

#include <memory>
#include <vector>
#include <string>

#include "OperationStatus.h"
#include "ChunkConsumer.h"
#include "BufferConsumer.h"


class Device;

namespace log_collector
{

    class NewLogCollector
    {
    public:
        static bool Tracer3ppLogsFilterOn;
        // should be called periodically until valid.
        bool PrepareForLogging();

        NewLogCollector(const std::string& deviceName, CpuType tracerType);
        NewLogCollector(const NewLogCollector& lg) = delete;
        NewLogCollector& operator=(const NewLogCollector& lc) = delete;
        ~NewLogCollector();

        OperationStatus ActivateLogging(LoggingType loggingType);
        void DeactivateLogging(LoggingType loggingType);

        bool IsLogging(LoggingType loggingType) const;

        // configuration functions
        OperationStatus SetModuleVerbosity(const std::string& module, const std::string& level);

        bool GetModuleLevelInfo(std::vector<std::pair<std::string, std::string> > &verbosityData);

        bool SetPollingInterval(unsigned pollingInterval) const;

        // for old api
        bool SetModuleVerbosityFromAssignment(const std::string& assignment);

        void Pause();
        void Resume();

        void RegisterPoller();
        void UnRegisterPoller();

    private:

        mutable std::mutex m_pollDestructionMutex;
        unsigned GetAhbStartAddress(BasebandType bb) const;
        unsigned GetLinkerStartAddress(BasebandType bb) const;
        bool ComputeLogBufferStartAddress();
        int GetBufferSizeInBytesById(int bufferSizeId) const;
        bool SetModuleVerbosityInner(const std::string& module, const std::string& level);
        bool SetMultipleModulesVerbosity(const std::string& modulesVerbosity);
        // configurations
        OperationStatus ApplyModuleVerbosity();

        bool GetModuleInfoFromDevice() const;
        // OS agnostic read log function
        bool ReadLogBuffer();

        void GetNextLogs(); // distribute next logs chunck (without log lines that were already read)

        void Poll();

        const std::string m_deviceName;
        const CpuType m_tracerCpuType;
        const std::string m_debugLogPrefix;

        unsigned m_pollerTaskId;
        //bool m_paused;
        bool m_valid;

        // log buffer members
        std::unique_ptr<log_buffer> m_logHeader; // content of log buffer header
        int m_logAddress; // log buffer start address
        std::vector<unsigned char> m_logBufferContent; // log buffer content
        unsigned m_logBufferSizeInDwords;

        std::unique_ptr<BufferConsumer> m_bufferConsumer;
        int m_prevLogOffset;
    };

}


